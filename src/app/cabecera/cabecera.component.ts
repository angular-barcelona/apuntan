import { Component, OnInit, Input } from '@angular/core';
import { Senyor } from '../senyor.interface';
import { SenyoresService } from '../senyores.service';

@Component({
  selector: 'apuntan-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {

  public senyores: Senyor[] = [];

  constructor(private senyoresService: SenyoresService) { }

  get nMarcados(): number {
    return this.getNMarcados();
  }

  public todosMarcados(): boolean {
    return this.nMarcados === this.senyores.length;
  }

  public toggleMarcados(e: string): void {
    if (this.todosMarcados()) {
      for (const senyor of this.senyores) {
        senyor.marcado = false;
      }
    } else {
      for (const senyor of this.senyores) {
        senyor.marcado = true;
      }
    }
  }

  public getNMarcados(): number {
    return this.senyores.filter(senyor => senyor.marcado).length;
  }

  ngOnInit() {
    this.senyores = this.senyoresService.senyores;
  }

}
