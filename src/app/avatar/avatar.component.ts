import { Component, OnInit, Input } from '@angular/core';
import { Senyor } from '../senyor.interface';

@Component({
  selector: 'apuntan-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent implements OnInit {

  @Input()
  public senyor: Senyor;

  constructor() { }

  ngOnInit() {
  }

}
