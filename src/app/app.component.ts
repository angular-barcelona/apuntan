import { Component, OnInit } from '@angular/core';
import { Senyor, SenyorAPI } from './senyor.interface';
import { Estados } from './estados.enum';
import { SenyoresService } from './senyores.service';

@Component({
  selector: 'apuntan-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public senyores: Senyor[];
  public nMarcados = 0;

  constructor(private senyoresService: SenyoresService) { }

  ngOnInit() {
    this.senyores = this.senyoresService.senyores;
  }

}
