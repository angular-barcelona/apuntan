import { Component, OnInit, Input } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { Senyor } from '../senyor.interface';
import { Estados } from '../estados.enum';

@Component({
  selector: 'apuntan-senyor',
  templateUrl: './senyor.component.html',
  styleUrls: ['./senyor.component.css']
})
export class SenyorComponent implements OnInit {

  @Input()
  public senyor: Senyor;
  public iconos = {
    check: faCheck
  };

  constructor() { }

  public cambiaMarcado(): void {
    if (this.senyor.marcado) {
      this.senyor.marcado = false;
    } else {
      this.senyor.marcado = true;
    }
  }

  public getEstado(): string {
    return Estados[this.senyor.estado];
  }

  ngOnInit() {
  }

}
