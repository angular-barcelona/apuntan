import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenyorComponent } from './senyor.component';

describe('SenyorComponent', () => {
  let component: SenyorComponent;
  let fixture: ComponentFixture<SenyorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenyorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenyorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
