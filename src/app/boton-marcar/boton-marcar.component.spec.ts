import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonMarcarComponent } from './boton-marcar.component';

describe('BotonMarcarComponent', () => {
  let component: BotonMarcarComponent;
  let fixture: ComponentFixture<BotonMarcarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotonMarcarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotonMarcarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
