import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Senyor } from '../senyor.interface';

@Component({
  selector: 'apuntan-boton-marcar',
  templateUrl: './boton-marcar.component.html',
  styleUrls: ['./boton-marcar.component.css']
})
export class BotonMarcarComponent implements OnInit {

  @Input()
  public todosMarcados: boolean;

  @Output()
  public cualquiera = new EventEmitter();

  constructor() { }

  public toggleMarcados(evento): void {
    evento.preventDefault();
    this.cualquiera.emit();
  }

  ngOnInit() {

  }

}
