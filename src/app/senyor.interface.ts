import { Estados } from './estados.enum';

export interface SenyorAPI {
  nombre: string;
  profesion: string;
  estado: Estados;
  twitter: string;
  careto: string;
}

export interface Senyor extends SenyorAPI {
  marcado: boolean;
  inicial: string;
}
