import { Injectable } from '@angular/core';
import { Senyor, SenyorAPI } from './senyor.interface';
import { Estados } from './estados.enum';

@Injectable({
  providedIn: 'root'
})
export class SenyoresService {

  public senyores: Senyor[];

  constructor() {
    const senyoresAPI: SenyorAPI[] = [
      {
        nombre: 'El Fary',
        profesion: 'Influencer',
        estado: 10,
        twitter: 'pendiente',
        careto: 'fary.jpg'
      },
      {
        nombre: 'Julio Iglesias',
        profesion: 'Youtuber',
        estado: 11,
        twitter: '@JulioIglesias',
        careto: 'julio.jpg'
      },
      {
        nombre: 'Bertín Osborne',
        profesion: 'Java Developer',
        estado: 11,
        twitter: '@BertinOsborne',
        careto: 'bertin.jpg'
      }
    ];

    this.senyores = senyoresAPI.map(
      senyorAPI => ({
        ...senyorAPI,
        estado: senyorAPI.estado === 10 ? Estados.RIP : Estados.Vivo,
        marcado: false,
        inicial: this.getInicial(senyorAPI.nombre)
      })
    );
  }

  private getInicial(nombre: string): string {
    const partesNombre = nombre.split(' ');
    if (partesNombre.length === 1 || partesNombre[0].length > 2) {
      return partesNombre[0][0].toUpperCase();
    } else {
      return partesNombre[1][0].toUpperCase();
    }
  }

}
